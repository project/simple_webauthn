<?php

namespace Drupal\simple_webauthn\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides controllers for login and register via HTTP requests.
 */
class SimpleWebAuthenticationController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Logs in a user.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   A response which contains the ID and CSRF token.
   */
  public function login(Request $request) {
    $content = $request->getContent();

    return new Response([]);
  }

  /**
   * Register the user.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   A response which contains the ID and CSRF token.
   */
  public function register(Request $request) {
    $content = $request->getContent();

    return new Response([]);
  }

}
